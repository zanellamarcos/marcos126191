package br.upf.pos.ecommerce.sessionbean;

import java.util.List;

import javax.ejb.Remote;

import br.upf.pos.ecommerce.beans.Categoria;

@Remote
public interface CategoriaBeanRemote {

	public abstract List<Categoria> getCategorias();
	public abstract void inserir(Categoria c) throws Exception;
	
}
