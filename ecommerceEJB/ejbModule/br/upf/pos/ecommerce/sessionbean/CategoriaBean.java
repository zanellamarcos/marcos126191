package br.upf.pos.ecommerce.sessionbean;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import br.upf.pos.ecommerce.beans.Categoria;
import br.upf.pos.ecommerce.dao.CategoriaDaoImpl;
import br.upf.pos.ecommerce.dao.GenericDaoImpl;

/**
 * Session Bean implementation class CategoriaBean
 */
@Stateless
@LocalBean
public class CategoriaBean implements CategoriaBeanRemote {

    /**
     * Default constructor. 
     */
    public CategoriaBean() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public List<Categoria> getCategorias() {
		return new GenericDaoImpl().getObjetos(Categoria.class);
	}

	@Override
	public void inserir(Categoria c) throws Exception {
		new GenericDaoImpl().gravar(c);
		
	}

}
