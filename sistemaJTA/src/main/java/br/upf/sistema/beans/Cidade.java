package br.upf.sistema.beans;

import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity implementation class for Entity: Cidade
 *
 */
@Entity
@Table(schema = "cadastros")
public class Cidade implements Serializable {

	@Version
	private Long versao;

	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "genCidade")
	@SequenceGenerator(name = "genCidade", sequenceName = "genCidade", schema = "cadastros", allocationSize = 1)
	private Long id;
	@Column(length = 50, nullable = false)
	@NotBlank(message="O nome da cidade é obrigatória.")
	@Length(min=3,max=50,message="A cidade deve ter entre {min} e {max} caracteres.")
	private String nome;
	@Column(length = 2, nullable = false)
	@Enumerated(EnumType.STRING)
	@NotNull(message="O estado é obrigatório.")
	private EstadoEnum estado;
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "Cidade [id=" + id + ", nome=" + nome + ", estado=" + estado
				+ "]";
	}

	public Cidade() {
		super();
	}

	public Cidade(Long id, String nome, EstadoEnum estado) {
		super();
		this.id = id;
		this.nome = nome;
		this.estado = estado;
	}
	


	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public EstadoEnum getEstado() {
		return this.estado;
	}

	public void setEstado(EstadoEnum estado) {
		this.estado = estado;
	}

	public Long getVersao() {
		return versao;
	}

	public void setVersao(Long versao) {
		this.versao = versao;
	}
	
	
}
