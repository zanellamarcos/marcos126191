package br.upf.sistema.desenvolvimento;

import javax.persistence.EntityManager;

import org.junit.Test;

import br.upf.sistema.beans.Cidade;
import br.upf.sistema.beans.PFisica;
import br.upf.sistema.util.JPAUtil;


public class PFisicaTeste {
	
	@Test
	public void inserir(){
		
		EntityManager em = JPAUtil.getEntityManager();
		
		//PFisica
		PFisica pf = new PFisica();
		pf.setNome("Marcos");
		pf.setIE("12345678911");
		pf.setEndereco("Rua São Paulo");
		pf.setNumero("53");
		pf.setComplemento("Casa");
		pf.setBairro("Monte Belo");
		pf.setCidade(em.find(Cidade.class,1L));
		pf.setCEP("95.340-000");
		pf.setFone("(54)9902-8476");
		pf.setEmail("zanella1410@gmail.com");
		pf.setSenha("123456");
		pf.setSituacao(true);
		pf.setRg("2047384711");
		pf.setCpf("982.180.180-34");
		
		
		//PERISTENCIA
		em.getTransaction().begin();
		em.persist(pf);
		em.getTransaction().commit();
		
		
	}

}
