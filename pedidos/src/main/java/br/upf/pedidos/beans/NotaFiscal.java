package br.upf.pedidos.beans;

import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity implementation class for Entity: NotaFiscal
 *
 */
@Entity
@Table(schema = "pedido")
public class NotaFiscal implements Serializable {

	@Version
	private Long versao;

	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "genNota")
	@SequenceGenerator(name = "genNota", sequenceName = "genNota", schema = "pedido", allocationSize = 1)
	private Long numNotaFiscal;
	@Temporal(TemporalType.DATE)
	@NotBlank(message = "O data da nota é obrigatória.")
	private Date dataNota;
	@Column(nullable = false, precision = 2)
	@NotNull(message = "O total da nota devem ser informado.")
	private Float total;
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "NotaFiscal [numNotaFiscal=" + numNotaFiscal + ", dataNota="
				+ dataNota + ", total=" + total + "]";
	}

	public NotaFiscal() {
		super();
	}

	public NotaFiscal(Long numNotaFiscal, Date dataNota, Float total) {
		super();
		this.numNotaFiscal = numNotaFiscal;
		this.dataNota = dataNota;
		this.total = total;
	}

	public Long getNumNotaFiscal() {
		return this.numNotaFiscal;
	}

	public void setNumNotaFiscal(Long numNotaFiscal) {
		this.numNotaFiscal = numNotaFiscal;
	}

	public Date getDataNota() {
		return this.dataNota;
	}

	public void setDataNota(Date dataNota) {
		this.dataNota = dataNota;
	}

	public Float getTotal() {
		return this.total;
	}

	public void setTotal(Float total) {
		this.total = total;
	}

	public Long getVersao() {
		return versao;
	}

	public void setVersao(Long versao) {
		this.versao = versao;
	}

}
