package br.upf.pedidos.beans;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.br.CPF;

/**
 * Entity implementation class for Entity: PFisica
 *
 */
@Entity
@Table(schema = "cadastros")
public class PFisica extends Pessoa implements Serializable {

	/*
	 * // Classe Pessoa
	 *  protected Long id; protected String nome; protected
	 * String IE; protected String endereco; protected String numero; protected
	 * String complemento; protected String bairro; protected Cidade cidade;
	 * protected String CEP; protected String fone; protected String email;
	 * protected String senha; protected Boolean situacao;
	 */

	@Column(length = 14, unique = true)
	// 999.999.999-99
	@NotBlank(message = "O RG é obrigatória.")
	private String rg;
	@Column(length = 14, unique = true)
	// 999.999.999-99
	@CPF(message = "O CPF inválido.")
	private String cpf;
	private static final long serialVersionUID = 1L;

	public PFisica() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PFisica(String rg, String cpf) {
		super();
		this.rg = rg;
		this.cpf = cpf;
	}

	public PFisica(Long id, String nome, String iE, String endereco,
			String numero, String complemento, String bairro, Cidade cidade,
			String cEP, String fone, String email, String senha,
			Boolean situacao, String rg, String cpf) {
		super(id, nome, iE, endereco, numero, complemento, bairro, cidade, cEP,
				fone, email, senha, situacao);

		this.rg = rg;
		this.cpf = cpf;
	}

	
	
	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

}
