package br.upf.pedidos.beans;

import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.Future;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity implementation class for Entity: Entrega
 *
 */
@Entity
@Table(schema = "cadastros")
public class Entrega implements Serializable {

	@Version
	private Long versao;

	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "genEntrega")
	@SequenceGenerator(name = "genEntrega", sequenceName = "genEntrega", schema = "cadastros", allocationSize = 1)
	private Long id;
	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	@NotBlank(message = "O data de entrega é obrigatória.")
	@Future(message = "A data de entrega dever ser futura.")
	private Date dataEntrega;
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "Entrega [id=" + id + ", dataEntrega=" + dataEntrega + "]";
	}

	public Entrega() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Entrega(Long id, Date dataEntrega) {
		super();
		this.id = id;
		this.dataEntrega = dataEntrega;
	}

	public Long getVersao() {
		return versao;
	}

	public void setVersao(Long versao) {
		this.versao = versao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataEntrega() {
		return dataEntrega;
	}

	public void setDataEntrega(Date dataEntrega) {
		this.dataEntrega = dataEntrega;
	}

}
