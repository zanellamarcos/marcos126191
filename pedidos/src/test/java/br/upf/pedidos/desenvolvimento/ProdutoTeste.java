package br.upf.pedidos.desenvolvimento;

import javax.persistence.EntityManager;

import org.junit.Test;

import br.upf.pedidos.beans.Produto;
import br.upf.pedidos.beans.UnidadeMedidaEnum;
import br.upf.pedidos.util.JPAUtil;

public class ProdutoTeste {
	
	@Test
	public void inserir(){
		EntityManager em = JPAUtil.getEntityManager();
		
		Produto p = new Produto();
		p.setNome("Milho");
		p.setDescricao("Milho em grãos");
		p.setUnidadeMedida(UnidadeMedidaEnum.KG);
		p.setPrecoCompra(50.00F);
		p.setPrecoVenda(55.00F);
		p.setEstoque(15000.00F);
		p.setEstoqueMinimo(10000.00F);
		//
		Produto p1 = new Produto();
		p1.setNome("Cangica");
		p1.setDescricao("Cangica Moida");
		p1.setUnidadeMedida(UnidadeMedidaEnum.KG);
		p1.setPrecoCompra(45.00F);
		p1.setPrecoVenda(48.00F);
		p1.setEstoque(12000.00F);
		p1.setEstoqueMinimo(8500.00F);
		
		em.getTransaction().begin();
		em.persist(p);
		em.persist(p1);
		em.getTransaction().commit();
	}

}
