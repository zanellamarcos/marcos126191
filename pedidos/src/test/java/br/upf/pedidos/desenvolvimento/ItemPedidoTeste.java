package br.upf.pedidos.desenvolvimento;

import javax.persistence.EntityManager;

import org.junit.Test;

import br.upf.pedidos.beans.ItemPedido;
import br.upf.pedidos.beans.Produto;
import br.upf.pedidos.beans.UnidadeMedidaEnum;
import br.upf.pedidos.util.JPAUtil;


public class ItemPedidoTeste {
	
	
	@Test 
	public void inserir(){
		EntityManager em = JPAUtil.getEntityManager();
		
		//ItemPedido
		ItemPedido i = new ItemPedido();
		i.setProduto(em.find(Produto.class,1L));
		i.setUnidadeMedida(UnidadeMedidaEnum.KG);
		i.setPreco(55.00F);
		i.setQuantidade(25.00F);
		i.setTotalItem(1375.00F);
		
		//PERSISTINDO
		em.getTransaction().begin();
		em.persist(i);
		em.getTransaction().commit();
		
	}
	

}
