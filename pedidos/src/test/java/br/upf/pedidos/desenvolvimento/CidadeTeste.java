package br.upf.pedidos.desenvolvimento;

import javax.persistence.EntityManager;

import org.junit.Test;

import br.upf.pedidos.beans.Cidade;
import br.upf.pedidos.beans.EstadoEnum;
import br.upf.pedidos.util.JPAUtil;

public class CidadeTeste {
	
	@Test
	public void inserir(){
		EntityManager em = JPAUtil.getEntityManager();
		// Cidade 1
		Cidade c = new Cidade();
		c.setNome("Nova Araçá");
		c.setEstado(EstadoEnum.RS);
		// Cidade 2
		Cidade c1= new Cidade();
		c1.setNome("Nova Bassano");
		c1.setEstado(EstadoEnum.RS);
		
		em.getTransaction().begin();
		em.persist(c);
		em.persist(c1);
		em.getTransaction().commit();
		em.close();
	}
	
	
	
	@Test
	public void excluir(){
		EntityManager em = JPAUtil.getEntityManager();
		em.getTransaction().begin();
		Cidade c = em.find(Cidade.class, 3L);
		em.remove(c);
		em.getTransaction().commit();
		em.close();
	}

}
