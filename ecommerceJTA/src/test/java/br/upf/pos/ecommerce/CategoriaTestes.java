package br.upf.pos.ecommerce;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Test;

import br.upf.pos.ecommerce.beans.Categoria;
import br.upf.pos.ecommerce.util.JPAUtil;

public class CategoriaTestes {

	@Test
	public void inserir(){
		Categoria c = new Categoria();
		c.setId(1L);
		c.setDescricao("Teclados");
		EntityManager em = JPAUtil.getEntityManager();
		em.getTransaction().begin();
		em.persist(c);
		em.getTransaction().commit();		
	}

	@Test
	public void inserirVarias(){
		EntityManager em = JPAUtil.getEntityManager();
		em.getTransaction().begin();
		em.persist(new Categoria(1L, "Teclados"));
		em.persist(new Categoria(2L, "Computadores"));
		em.persist(new Categoria(3L, "Monitores"));
		em.persist(new Categoria(4L, "Impressoras"));
		em.persist(new Categoria(5L, "Mouses"));
		em.getTransaction().commit();		
	}	
	
	@Test
	public void alterar(){
		EntityManager em = JPAUtil.getEntityManager();
		em.getTransaction().begin();
		Categoria c = em.find(Categoria.class, 3L);
		c.setDescricao("Notebooks");
		em.getTransaction().commit();
	}
	
	@Test
	public void excluir(){
		EntityManager em = JPAUtil.getEntityManager();
		em.getTransaction().begin();
		Categoria c = em.find(Categoria.class, 4L);
		em.remove(c);
		em.getTransaction().commit();
	}
	
	@Test
	public void consultar(){
		EntityManager em = JPAUtil.getEntityManager();
		List<Categoria> lista = em.createQuery("from Categoria order by descricao").getResultList();
		for(Categoria c : lista)
			System.out.println(c.getId()+" - "+c.getDescricao());
	}
	
	
	
}
