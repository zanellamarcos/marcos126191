package br.upf.pos.ecommerce;

import java.util.List;

import org.junit.Test;

import br.upf.pos.ecommerce.beans.Categoria;
import br.upf.pos.ecommerce.dao.CategoriaDaoImpl;

public class CategoriaTesteDao {

	@Test
	public void testeListar(){
		List<Categoria>	list = new CategoriaDaoImpl().getObjetos();
		System.out.println(list);
	}
	
	@Test 
	public void inserir() throws Exception{
		new CategoriaDaoImpl().gravar(new Categoria(6L, "Categoria 6"));
	}
	
}
