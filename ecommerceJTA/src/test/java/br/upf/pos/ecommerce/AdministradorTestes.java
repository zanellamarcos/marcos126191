package br.upf.pos.ecommerce;

import javax.persistence.EntityManager;

import org.junit.Test;

import br.upf.pos.ecommerce.beans.Administrador;
import br.upf.pos.ecommerce.beans.Categoria;
import br.upf.pos.ecommerce.beans.Produto;
import br.upf.pos.ecommerce.util.JPAUtil;

public class AdministradorTestes {

	@Test
	public void inserir(){
		EntityManager em = JPAUtil.getEntityManager();
		
		Administrador p = new Administrador();
        p.setEmail("teste");
        p.setNome("Admin");
        p.setPermissaoCategoria(true);
        p.setPermissaoConsultas(true);
        p.setPermissaoCupom(true);
        p.setPermissaoProduto(false);
		p.setSenha("123");
		
		em.getTransaction().begin();
		em.persist(p);
		em.getTransaction().commit();
	}
	
	
	
	
}
