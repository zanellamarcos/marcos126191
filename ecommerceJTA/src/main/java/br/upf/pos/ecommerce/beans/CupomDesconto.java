package br.upf.pos.ecommerce.beans;

import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;
import java.lang.Float;
import java.lang.Integer;
import java.lang.Long;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: CupomDesconto
 *
 */
@Entity
@Table(schema = "cadastros")
public class CupomDesconto implements Serializable {
	@Version
	private Long versao;

	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "genCupomDesconto")
	@SequenceGenerator(name = "genCupomDesconto", sequenceName = "genCupomDesconto", schema = "cadastros", allocationSize = 1)
	private Long id;
	@Temporal(TemporalType.DATE)
	private Date dataInicial;
	@Temporal(TemporalType.DATE)
	private Date dataFinal;
	@Column(nullable=false)
	private Float valorInicial;
	@Column(nullable=false)
	private Float valorFinal;
	@Column(nullable=false)
	private Integer quantidadeCupons;
	@Column(nullable=false)
	private Integer quantidadeUsada;
	@Column(nullable=false)
	private Float percentualDesconto;
	private static final long serialVersionUID = 1L;

	public CupomDesconto() {
		super();
	}   
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public Date getDataInicial() {
		return this.dataInicial;
	}

	public void setDataInicial(Date dateInicial) {
		this.dataInicial = dateInicial;
	}   
	public Date getDataFinal() {
		return this.dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}   
	public Float getValorInicial() {
		return this.valorInicial;
	}

	public void setValorInicial(Float valorInicial) {
		this.valorInicial = valorInicial;
	}   
	public Float getValorFinal() {
		return this.valorFinal;
	}

	public void setValorFinal(Float valorFinal) {
		this.valorFinal = valorFinal;
	}   
	public Integer getQuantidadeCupons() {
		return this.quantidadeCupons;
	}

	public void setQuantidadeCupons(Integer quantidadeCupons) {
		this.quantidadeCupons = quantidadeCupons;
	}   
	public Integer getQuantidadeUsada() {
		return this.quantidadeUsada;
	}

	public void setQuantidadeUsada(Integer quantidadeUsada) {
		this.quantidadeUsada = quantidadeUsada;
	}   
	public Float getPercentualDesconto() {
		return this.percentualDesconto;
	}

	public void setPercentualDesconto(Float percentualDesconto) {
		this.percentualDesconto = percentualDesconto;
	}
	public Long getVersao() {
		return versao;
	}
	public void setVersao(Long versao) {
		this.versao = versao;
	}

   
}
