package br.upf.pos.ecommerce.beans;

import br.upf.pos.ecommerce.beans.Pessoa;

import java.io.Serializable;
import java.lang.Boolean;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Administrador
 *
 */
@Entity
@Table(schema = "cadastros")
public class Administrador extends Pessoa implements Serializable {
	
	@Column(nullable=false)
	private Boolean permissaoCategoria;
	@Column(nullable=false)
	private Boolean permissaoProduto;
	@Column(nullable=false)
	private Boolean permissaoCupom;
	@Column(nullable=false)
	private Boolean permissaoConsultas;
	private static final long serialVersionUID = 1L;

	public Administrador() {
		super();
	}   
	public Boolean getPermissaoCategoria() {
		return this.permissaoCategoria;
	}

	public void setPermissaoCategoria(Boolean permissaoCategoria) {
		this.permissaoCategoria = permissaoCategoria;
	}   
	public Boolean getPermissaoProduto() {
		return this.permissaoProduto;
	}

	public void setPermissaoProduto(Boolean permissaoProduto) {
		this.permissaoProduto = permissaoProduto;
	}   
	public Boolean getPermissaoCupom() {
		return this.permissaoCupom;
	}

	public void setPermissaoCupom(Boolean permissaoCupom) {
		this.permissaoCupom = permissaoCupom;
	}   
	public Boolean getPermissaoConsultas() {
		return this.permissaoConsultas;
	}

	public void setPermissaoConsultas(Boolean permissaoConsultas) {
		this.permissaoConsultas = permissaoConsultas;
	}
   
}
