package br.upf.pos.ecommerce.beans;

import br.upf.pos.ecommerce.beans.Pessoa;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;

import javax.persistence.*;
import static javax.persistence.FetchType.LAZY;

/**
 * Entity implementation class for Entity: Cliente
 *
 */
@Entity
@Table(schema = "cadastros")
@NamedQueries({
		
	@NamedQuery(name = "obterClientePorNome", query = "select c from Cliente c where c.nome=:nome"),
	@NamedQuery(name = "obterClientePorCpf", query = "select c from Cliente c where c.cpf=:cpf and c.cnpj is null "),
	@NamedQuery(name = "obterClientePorCnpj", query = "select c from Cliente c where c.cnpj=:cnpj") 
})
public class Cliente extends Pessoa implements Serializable {
	@Column(length = 20, nullable=false)
	private String fone;
	@Column(length = 14, unique=true) // 999.999.999-99
	private String cpf;
	@Column(length = 18, unique=true) // 99.999.999/9999-99
	private String cnpj;
	@Column(length = 60, nullable=false)
	private String endereco;
	@Column(length = 10, nullable=false)
	private String numero;
	@Column(length = 30)
	private String complemento;
	@Column(length = 50, nullable=false)
	private String bairro;
	@Column(length = 50, nullable=false)
	private String cidade;
	@Column(length = 2, nullable=false)
	@Enumerated(EnumType.STRING)
	private UFEnum estado;
	@Column(length = 10, nullable=false) // 99.999-999
	private String cep;
	@Temporal(TemporalType.DATE)
	@Basic(fetch = LAZY)
	private Date dataNascimento;
	@Transient
	private Integer idade;
	private static final long serialVersionUID = 1L;

	public Cliente() {
		super();
	}   
	public String getFone() {
		return this.fone;
	}

	public void setFone(String fone) {
		this.fone = fone;
	}   
	public String getCpf() {
		return this.cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}   
	public String getCnpj() {
		return this.cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}   
	public String getEndereco() {
		return this.endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}   
	public String getNumero() {
		return this.numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}   
	public String getComplemento() {
		return this.complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}   
	public String getBairro() {
		return this.bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}   
	public String getCidade() {
		return this.cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}   
	public UFEnum getEstado() {
		return this.estado;
	}

	public void setEstado(UFEnum estado) {
		this.estado = estado;
	}   
	public String getCep() {
		return this.cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public Integer getIdade() {
		// calcular idade...
		return idade;
	}
	public void setIdade(Integer idade) {
		this.idade = idade;
	}
   
}
