package br.upf.pos.ecommerce.beans;

import java.io.Serializable;
import java.lang.Long;
import java.lang.String;

import javax.persistence.*;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity implementation class for Entity: Categoria
 *
 */
@Entity
@Table(name="tbl_Categoria", schema = "cadastros")
public class Categoria implements Serializable {
	@Override
	public String toString() {
		return id + " " + descricao;
	}

	@Version
	private Long versao;
	
	@Id
	private Long id;
	@Column(unique = true, nullable = false, length = 50)
	@NotBlank(message="A descrição da categoria é obrigatória.")
	@Length(min=3, max=50, message="A descrição deve ter entre {min} e {max} caracteres.")
	private String descricao;
	private static final long serialVersionUID = 1L;
	
	public Categoria(Long id, String descricao) {
		super();
		this.id = id;
		this.descricao = descricao;
	}
	
	public Categoria() {
		super();
	}   
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getVersao() {
		return versao;
	}

	public void setVersao(Long versao) {
		this.versao = versao;
	}


	
}
