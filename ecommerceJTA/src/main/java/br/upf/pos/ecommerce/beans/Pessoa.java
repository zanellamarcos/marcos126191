package br.upf.pos.ecommerce.beans;

import java.io.Serializable;
import java.lang.Long;
import java.lang.String;

import javax.persistence.*;

import static javax.persistence.GenerationType.SEQUENCE;
import static javax.persistence.InheritanceType.JOINED;

/**
 * Entity implementation class for Entity: Pessoa
 *
 */
@Entity
@Inheritance(strategy = JOINED)
@Table(schema = "cadastros")
public abstract class Pessoa implements Serializable {
	@Version
	private Long versao;
	   
	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "genPessoa")
	@SequenceGenerator(name = "genPessoa", sequenceName = "genPessoa", schema = "cadastros", allocationSize = 1)
	private Long id;
	@Column(nullable = false, length = 60)
	private String nome;
	@Column(unique = true, nullable = false, length = 80)
	private String email;
	@Column(nullable = false, length = 50)
	private String senha;
	private static final long serialVersionUID = 1L;

	public Pessoa() {
		super();
	}   
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}   
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}   
	public String getSenha() {
		return this.senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	public Long getVersao() {
		return versao;
	}
	public void setVersao(Long versao) {
		this.versao = versao;
	}


}
