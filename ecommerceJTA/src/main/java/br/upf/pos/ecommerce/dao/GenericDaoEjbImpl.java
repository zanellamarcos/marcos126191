package br.upf.pos.ecommerce.dao;

import java.util.List;

import javax.persistence.EntityManager;

public class GenericDaoEjbImpl implements GenericDao {

	private EntityManager em;
	
	public GenericDaoEjbImpl(EntityManager em) {
		super();
		this.em = em;
	}	
	
	@Override
	public <T> T gravar(T objeto) throws Exception {
		objeto = em.merge(objeto);
		return objeto;
	}

	@Override
	public <T> void excluir(Class<T> classe, Long id) throws Exception {
		em.remove(em.find(classe, id));
	}

	@Override
	public <T> T getObjetoById(Class<T> classe, Long id) {
		T ret = em.find(classe, id);
		return ret;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> getObjetos(Class<T> classe) {
		List<T> ret = em.createQuery("select o from "+
	                  classe.getSimpleName()+" o").getResultList();
		return ret;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> getObjetos(Class<T> classe, String atributoOrdem) {
		List<T> ret = em.createQuery(
				"select o from "+
	            classe.getSimpleName()+" o order by "+atributoOrdem).getResultList();
		return ret;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> getObjetos(Class<T> classe, String atributoOrdem,
			int maxResults, int firstResult) {
		List<T> ret = em.createQuery(
				"select o from "+
	            classe.getSimpleName()+" o order by "+atributoOrdem).
				setMaxResults(maxResults).setFirstResult(firstResult).getResultList();
		return ret;
	}

}
