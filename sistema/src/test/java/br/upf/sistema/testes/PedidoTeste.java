package br.upf.sistema.testes;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import br.upf.sistema.beans.ItemPedido;
import br.upf.sistema.beans.Pedido;
import br.upf.sistema.beans.UnidadeMedidaEnum;
import br.upf.sistema.controller.InserirPedido;
import br.upf.sistema.dao.GenericDaoImpl;
import br.upf.sistema.util.ValidateUtil;

//CLASSE DE TESTE PROFESSOR ÉDIPO
public class PedidoTeste {

	// SIMULA OPERACOES COM O MOCKITO
	@Test
	public void testaInserirPedido() throws Exception {
		GenericDaoImpl dao = Mockito.mock(GenericDaoImpl.class);
		InserirPedido ipedido = new InserirPedido(dao);
		// ItemPedido(id, produto, unidadeMedida, preco, quantidade, totalItem,
		// pedido)
		ItemPedido item = new ItemPedido(null, "Soja", UnidadeMedidaEnum.KG,
				0.85F, 25000.00F, 21250F, "123");
		Pedido p = ipedido.grava(item);
		Mockito.verify(dao).gravar(p);

	}

	// Verifica como o sistema esta validando se forem deixados campos
	// obrigatorios nulos
	@Test
	public void PedidolTestaNull() {
		GenericDaoImpl dao = Mockito.mock(GenericDaoImpl.class);
		// Pedido(id, pessoa, entrega, pagamento, dataPedido, itens, parcelas,
		// status, nota, totalPedido);
		Pedido p = new Pedido(null, null, null, null, null, null, null, null,
				null, null);
		List<String> erros = ValidateUtil.validationToStringList(p);
		Assert.assertEquals(9, erros.size());
		Assert.assertEquals("A entrega deve ser informada.", erros.get(0));
		Assert.assertEquals("O(s) item(ns) do pedido devem ser informado(s).",
				erros.get(1));
		Assert.assertEquals("A nota fiscal devem ser informada.", erros.get(2));
		Assert.assertEquals("As parcelas devem ser informadas.", erros.get(3));
		Assert.assertEquals("O status deve ser informado.", erros.get(4));
		Assert.assertEquals("A data dever ser informada.", erros.get(5));
		Assert.assertEquals("A pessoa deve ser informada.", erros.get(6));
		Assert.assertEquals("O total do pedido devem ser informado.",
				erros.get(7));
		Assert.assertEquals("O pagamento deve ser informado.", erros.get(8));

	}

}
