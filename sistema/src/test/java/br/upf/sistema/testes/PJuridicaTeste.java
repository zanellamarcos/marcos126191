package br.upf.sistema.testes;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import br.upf.sistema.beans.Cidade;
import br.upf.sistema.beans.EstadoEnum;
import br.upf.sistema.beans.PJuridica;
import br.upf.sistema.beans.Pessoa;
import br.upf.sistema.controller.InserirPFisica;
import br.upf.sistema.controller.InserirPJuridica;
import br.upf.sistema.dao.GenericDaoImpl;
import br.upf.sistema.util.ValidateUtil;

//CLASSE DE TESTE PROFESSOR ÉDIPO
public class PJuridicaTeste {

	// SIMULA OPERACOES COM O MOCKITO
	@Test
	public void testaInserirPJuridica() throws Exception {
		GenericDaoImpl dao = Mockito.mock(GenericDaoImpl.class);
		InserirPJuridica ipj = new InserirPJuridica(dao);
		Cidade c = new Cidade(null, "Nova Bassano", EstadoEnum.RS);
		PJuridica pj = ipj.grava(c);
		Mockito.verify(dao).gravar(pj);

	}

	// Verifica como o sistema esta validando CNPJ e Emails
	@Test
	public void PJuridicaValidaCnpjEmail() {
		GenericDaoImpl dao = Mockito.mock(GenericDaoImpl.class);
		InserirPFisica ipf = new InserirPFisica(dao);
		Cidade c = new Cidade(null, "Nova Bassano", EstadoEnum.RS);
		PJuridica pj = new PJuridica(null, "Tetometal", "12345678911",
				"Rua São Paulo", "53", "Casa", "Monte Belo", c, "95.340-000",
				"(54)9902-8476", "terra", "123456", true, "09.126.540/0001-80");
		List<String> erros = ValidateUtil.validationToStringList(pj);
		Assert.assertEquals(2, erros.size());
		Assert.assertEquals("CNPJ e invalido.", erros.get(0));
		Assert.assertEquals("O Email inválido.", erros.get(1));
	}


	// Verifica como o sistema esta validando se forem deixados campos
	// obrigatorios nulos
	@Test
	public void pJuridicaTestaNull() {
		GenericDaoImpl dao = Mockito.mock(GenericDaoImpl.class);
		InserirPJuridica ipj = new InserirPJuridica(dao);
		Cidade c = new Cidade(null, "Nova Bassano", EstadoEnum.RS);
		// PJuridica pj = new PJuridica(id, nome, iE, endereco, numero,
		// complemento, bairro, cidade, cEP, fone, email, senha, situacao,
		// cnpj);
		PJuridica pj = new PJuridica(null, null, null, null, null, "Casa",
				null, null, null, null, null, null, null, null);
		List<String> erros = ValidateUtil.validationToStringList(pj);
		Assert.assertEquals(12, erros.size());
		Assert.assertEquals("O CNPJ e obrigatorio.", erros.get(0));
		Assert.assertEquals("O endereço da pessoa e obrigatorio.", erros.get(1));
		Assert.assertEquals("A senha é obrigatória.", erros.get(2));
		Assert.assertEquals("O nome da pessoa e obrigatorio.", erros.get(3));
		Assert.assertEquals("O CEP é obrigatório.", erros.get(4));
		Assert.assertEquals("A situação de ser informada.", erros.get(5));
		Assert.assertEquals("O numero é obrigatório.", erros.get(6));
		Assert.assertEquals("A Inscricao Estadual e obrigatoria.", erros.get(7));
		Assert.assertEquals("O Email e obrigatorio.", erros.get(8));
		Assert.assertEquals("A cidade da pessoa é obrigatória.", erros.get(9));
		Assert.assertEquals("O bairro é obrigatório.", erros.get(10));
		Assert.assertEquals("O Fone é obrigatório.", erros.get(11));

	}

	// Verifica como o sistema esta validando a quantidade de caracteres mínimos
	@Test
	public void pJuridicaTestaCaracteres() {
		GenericDaoImpl dao = Mockito.mock(GenericDaoImpl.class);
		InserirPJuridica ipj = new InserirPJuridica(dao);
		Cidade c = new Cidade(null, "Nova Bassano", EstadoEnum.RS);
		// PJuridica pj = new PJuridica(id, nome, iE, endereco, numero,
		// complemento, bairro, cidade, cEP, fone, email, senha, situacao,
		// cnpj);
		PJuridica pj = new PJuridica(null, "Te", "12345678911", "RS", "", "Ca",
				"Ba", c, "95.340-000", "(54)9902-8476",
				"sulmeta@sulmeta.com.br", "12345", true, "09.126.540/0001-90");
		List<String> erros = ValidateUtil.validationToStringList(pj);
		Assert.assertEquals(7, erros.size());

		Assert.assertEquals("A senha deve ter entre 6 e 50 caracteres.",
				erros.get(0));
		Assert.assertEquals("O complemento deve ter entre 3 e 30 caracteres.",
				erros.get(1));
		Assert.assertEquals("O numero deve ter entre 1 e 10 caracteres.",
				erros.get(2));
		Assert.assertEquals("O numero é obrigatório.", erros.get(3));
		Assert.assertEquals("O bairro deve ter entre 3 e 50 caracteres.",
				erros.get(4));
		Assert.assertEquals("O endereço deve ter entre 3 e 60 caracteres.",
				erros.get(5));
		Assert.assertEquals(
				"O nome da pessoa deve ter entre 3 e 60 caracteres.",
				erros.get(6));

	}
	

	// TESTES NO BANCO DE DADOS
	@Test
	public void inserirPJuridica() throws Exception {
		Cidade c = new GenericDaoImpl().getObjetoById(Cidade.class, 1L);
		PJuridica pj = new PJuridica(null, "Tetometal", "12345678911",
				"Rua São Paulo", "53", "Casa", "Monte Belo", c, "95.340-000",
				"(54)9902-8476", "sulmeta@sulmeta.com.br", "123456", true,
				"09.126.540/0001-90");
		pj = new GenericDaoImpl().gravar(pj);
		System.out.println(pj);
	}

	@Test
	public void listarPJuridica() {
		List<PJuridica> lista = new GenericDaoImpl()
				.getObjetos(PJuridica.class);
		System.out.println(lista);
	}

	@Test
	public void listarPessoa() {
		List<Pessoa> lista = new GenericDaoImpl().getObjetos(Pessoa.class);
		System.out.println(lista);
	}

	@Test
	public void alterarPJuridica() throws Exception {
		PJuridica pj = new GenericDaoImpl().getObjetoById(PJuridica.class, 12L);
		pj.setEmail("sulmeta@sulmeta.com.br");
		pj.setNome("Sulmeta Corp");
		new GenericDaoImpl().gravar(pj);
		listarPJuridica();
	}

	@Test
	public void excluirPFisica() throws Exception {
		new GenericDaoImpl().excluir(PJuridica.class, 12L);
		listarPJuridica();
	}

}
