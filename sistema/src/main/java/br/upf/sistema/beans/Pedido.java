package br.upf.sistema.beans;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity implementation class for Entity: Pedido
 *
 */
@Entity
@Table(schema = "pedido")
public class Pedido implements Serializable {

	@Version
	private Long versao;

	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "genPedido")
	@SequenceGenerator(name = "genPedido", sequenceName = "genPedido", schema = "pedido", allocationSize = 1)
	private Long id;
	@ManyToOne(optional = false)
	@NotNull(message = "A pessoa deve ser informada.")
	private Pessoa pessoa;
	@ManyToOne(optional = false)
	@NotNull(message = "A entrega deve ser informada.")
	private Entrega entrega;
	@Column(length = 8, nullable = false)
	@Enumerated(EnumType.STRING)
	@NotNull(message = "O pagamento deve ser informado.")
	private PagamentoEnum pagamento;
	@Temporal(TemporalType.DATE)
	@NotNull(message = "A data dever ser informada.")
	private Date dataPedido;
	@OneToMany(cascade = ALL, orphanRemoval = true, mappedBy = "pedido", fetch = LAZY)
	@OrderColumn(name = "id")
	@NotNull(message = "O(s) item(ns) do pedido devem ser informado(s).")
	@Size(min = 1, message = "Deve ter no mínimo {min} item de pedido.")
	private List<ItemPedido> itens;
	@Column(nullable = false)
	@NotNull(message = "As parcelas devem ser informadas.")
	private Integer parcelas;
	@Column(length = 9, nullable = false)
	@Enumerated(EnumType.STRING)
	@NotNull(message = "O status deve ser informado.")
	private StatusEnum status;
	@OneToOne(optional = false)
	@NotNull(message = "A nota fiscal devem ser informada.")
	private NotaFiscal nota;
	@Column(nullable = false, precision = 2)
	@NotNull(message = "O total do pedido devem ser informado.")
	private Float totalPedido;
	private static final long serialVersionUID = 1L;


	@Override
	public String toString() {
		return "Pedido [versao=" + versao + ", id=" + id + ", pessoa=" + pessoa
				+ ", entrega=" + entrega + ", pagamento=" + pagamento
				+ ", dataPedido=" + dataPedido + ", itens=" + itens
				+ ", parcelas=" + parcelas + ", status=" + status + ", nota="
				+ nota + ", totalPedido=" + totalPedido + "]";
	}
	
	

	public Pedido() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Pedido(Long id, Pessoa pessoa, Entrega entrega,
			PagamentoEnum pagamento, Date dataPedido, List<ItemPedido> itens,
			Integer parcelas, StatusEnum status, NotaFiscal nota,
			Float totalPedido) {
		super();
		this.id = id;
		this.pessoa = pessoa;
		this.entrega = entrega;
		this.pagamento = pagamento;
		this.dataPedido = dataPedido;
		this.itens = itens;
		this.parcelas = parcelas;
		this.status = status;
		this.nota = nota;
		this.totalPedido = totalPedido;
	}

	

	public Pedido(Long id, Pessoa pessoa, Entrega entrega,
			PagamentoEnum pagamento, Date dataPedido, Integer parcelas,
			StatusEnum status, NotaFiscal nota, Float totalPedido) {
		super();
		this.id = id;
		this.pessoa = pessoa;
		this.entrega = entrega;
		this.pagamento = pagamento;
		this.dataPedido = dataPedido;
		this.parcelas = parcelas;
		this.status = status;
		this.nota = nota;
		this.totalPedido = totalPedido;
	}



	public Pedido(Object object, String string, String string2,
			PagamentoEnum dinheiro, int i, StatusEnum atendido, int j, float f) {
		// TODO Auto-generated constructor stub
	}



	public Pedido(Long id2, UnidadeMedidaEnum unidadeMedida, Float preco,
			Float quantidade, Float totalItem, Produto produto, Pedido pedido) {
		// TODO Auto-generated constructor stub
	}



	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pessoa getPessoa() {
		return this.pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Entrega getEntrega() {
		return this.entrega;
	}

	public void setEntrega(Entrega entrega) {
		this.entrega = entrega;
	}

	public PagamentoEnum getPagamento() {
		return this.pagamento;
	}

	public void setPagamento(PagamentoEnum pagamento) {
		this.pagamento = pagamento;
	}

	public Date getDataPedido() {
		return this.dataPedido;
	}

	public void setDataPedido(Date dataPedido) {
		this.dataPedido = dataPedido;
	}

	public Integer getParcelas() {
		return this.parcelas;
	}

	public void setParcelas(Integer parcelas) {
		this.parcelas = parcelas;
	}
	
	
	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}

	public Float getTotalPedido() {
		return this.totalPedido;
	}

	public void setTotalPedido(Float totalPedido) {
		this.totalPedido = totalPedido;
	}

	public Long getVersao() {
		return versao;
	}

	public void setVersao(Long versao) {
		this.versao = versao;
	}

	public List<ItemPedido> getItens() {
		return itens;
	}

	public void setItens(List<ItemPedido> itens) {
		this.itens = itens;
	}

	public NotaFiscal getNota() {
		return nota;
	}

	public void setNota(NotaFiscal nota) {
		this.nota = nota;
	}

	
	
	
}
