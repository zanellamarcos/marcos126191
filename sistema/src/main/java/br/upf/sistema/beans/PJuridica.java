package br.upf.sistema.beans;

import java.io.Serializable;
import java.lang.String;

import javax.persistence.*;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.br.CNPJ;

/**
 * Entity implementation class for Entity: PJuridica
 *
 */
@Entity
@Table(schema = "cadastros")
public class PJuridica extends Pessoa implements Serializable {
		
	@Column(length = 18, unique = true, nullable=false)
	// 99.999.999/9999-99
	@CNPJ(message="CNPJ e invalido.")
	@NotBlank(message="O CNPJ e obrigatorio.")
	private String cnpj;
	private static final long serialVersionUID = 1L;

		
	public PJuridica(Long id, String nome, String iE, String endereco,
			String numero, String complemento, String bairro, Cidade cidade,
			String cEP, String fone, String email, String senha,
			Boolean situacao, String cnpj) {
		super(id, nome, iE, endereco, numero, complemento, bairro, cidade, cEP, fone,
				email, senha, situacao);
		this.cnpj = cnpj;
		
	}
	
	
	@Override
	public String toString() {
		return "PJuridica [id=" + getId() + ", nome=" + getNome() + ", cnpj="
				+ cnpj + ", IE=" + getIE() + ", endereco=" + getEndereco()
				+ ", numero=" + getNumero() + ", complemento="
				+ getComplemento() + ", bairro=" + getBairro() + ", cidade="
				+ getCidade() + ", CEP=" + getCEP() + ", fone=" + getFone()
				+ ", email=" + getEmail() + ", senha=" + getSenha()
				+ ", situacao=" + getSituacao() + " ]";

	}
	

	

	public PJuridica() {
		super();
	}

	public PJuridica(String cnpj) {
		super();
		this.cnpj = cnpj;
	}

	public PJuridica(Long id, String nome, EstadoEnum estado) {
		// TODO Auto-generated constructor stub
	}


	public String getCnpj() {
		return this.cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

}
