package br.upf.sistema.beans;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.br.CPF;

/**
 * Entity implementation class for Entity: PFisica
 *
 */
@Entity
@Table(schema = "cadastros")
public class PFisica extends Pessoa implements Serializable {

	@Column(length = 14, unique = true, nullable=false)
	// 999.999.999-99
	@NotBlank(message = "O RG é obrigatório.")
	private String rg;
	@Column(length = 14, unique = true, nullable=false)
	// 999.999.999-99
	@CPF(message="O CPF e invalido.")
	@NotBlank(message="O CPF e obrigatorio.")
	private String cpf;
	private static final long serialVersionUID = 1L;

	public PFisica() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PFisica(Long id, String nome, String iE, String endereco,
			String numero, String complemento, String bairro, Cidade cidade,
			String cEP, String fone, String email, String senha,
			Boolean situacao, String rg, String cpf) {
		super(id, nome, iE, endereco, numero, complemento, bairro, cidade, cEP,
				fone, email, senha, situacao);

		this.rg = rg;
		this.cpf = cpf;
	}


	
	 public PFisica(Long id, String nome, EstadoEnum estado) {
		// TODO Auto-generated constructor stub
	}

	

	@Override
	 public String toString() { 
		 return "PFisica [id=" + getId() +
	  ", nome=" + getNome() + ", cpf=" + cpf + ", IE=" + getIE() + ", rg=" + rg +
	  ", endereco=" + getEndereco() + ", numero=" + getNumero() + ", complemento=" +
	  getComplemento() + ", bairro=" + getBairro() + ", cidade=" + getCidade() + ", CEP=" +
	  getCEP() + ", fone=" + getFone()+ ", email=" + getEmail() + ", senha=" + getSenha() +
	  ", situacao=" + getSituacao() + " ]";
	  
	  }
	
	 

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

}
