package br.upf.sistema.beans;

import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity implementation class for Entity: ItemPedido
 *
 */
@Entity
@Table(schema = "pedido")
public class ItemPedido implements Serializable {

	@Version
	private Long versao;

	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "genItemPedido")
	@SequenceGenerator(name = "genItemPedido", sequenceName = "genItemPedido", schema = "pedido", allocationSize = 1)
	private Long id;
	@ManyToOne(optional = false)
	@NotNull(message = "O Produto deve ser informado.")
	private Produto produto;
	@Column(length = 2, nullable = false)
	@Enumerated(EnumType.STRING)
	@NotBlank(message = "A unidade de medida é obrigatória.")
	private UnidadeMedidaEnum unidadeMedida;
	@Column(nullable = false, precision = 2)
	@NotBlank(message = "O preço é obrigatório.")
	private Float preco;
	@Column(nullable = false, precision = 2)
	@NotBlank(message = "A quantidade é obrigatória.")
	private Float quantidade;
	@Column(nullable = false, precision = 2)
	@NotBlank(message = "O total do item é obrigatório.")
	private Float totalItem;
	@ManyToOne(optional = false)
	@NotNull(message = "O pedido devem ser informado.")
	private Pedido pedido;
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "ItemPedido [id=" + id + ", produto=" + produto
				+ ", unidadeMedida=" + unidadeMedida + ", preco=" + preco
				+ ", quantidade=" + quantidade + ", totalItem=" + totalItem
				+ ", pedido=" + pedido + "]";
	}

	public ItemPedido() {
		super();
	}

	public ItemPedido(Long id, Produto produto,
			UnidadeMedidaEnum unidadeMedida, Float preco, Float quantidade,
			Float totalItem, Pedido pedido) {
		super();
		this.id = id;
		this.produto = produto;
		this.unidadeMedida = unidadeMedida;
		this.preco = preco;
		this.quantidade = quantidade;
		this.totalItem = totalItem;
		this.pedido = pedido;
	}

	public ItemPedido(Object object, String string, UnidadeMedidaEnum kg,
			float f, float g, float h, String string2) {
		// TODO Auto-generated constructor stub
	}

	public ItemPedido(Long id2, String nome, String descricao,
			UnidadeMedidaEnum unidadeMedida2, Float precoCompra,
			Float precoVenda, Float estoque, Float estoqueMinimo) {
		// TODO Auto-generated constructor stub
	}

	

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Produto getProduto() {
		return this.produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public UnidadeMedidaEnum getUnidadeMedida() {
		return this.unidadeMedida;
	}

	public void setUnidadeMedida(UnidadeMedidaEnum unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

	public Float getPreco() {
		return this.preco;
	}

	public void setPreco(Float preco) {
		this.preco = preco;
	}

	public Float getQuantidade() {
		return this.quantidade;
	}

	public void setQuantidade(Float quantidade) {
		this.quantidade = quantidade;
	}

	public Float getTotalItem() {
		return this.totalItem;
	}

	public void setTotalItem(Float totalItem) {
		this.totalItem = totalItem;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public Long getVersao() {
		return versao;
	}

	public void setVersao(Long versao) {
		this.versao = versao;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
}
