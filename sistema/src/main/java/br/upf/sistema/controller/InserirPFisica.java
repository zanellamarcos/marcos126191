package br.upf.sistema.controller;

import br.upf.sistema.beans.Cidade;
import br.upf.sistema.beans.PFisica;
import br.upf.sistema.dao.GenericDaoImpl;

public class InserirPFisica {

	private GenericDaoImpl dao;

	
	public InserirPFisica(GenericDaoImpl dao) {
		this.dao = dao;
	}
	
	
	public PFisica grava(Cidade cidade) throws Exception{
		PFisica pf = new PFisica(
				cidade.getId(),
				cidade.getNome(),
				cidade.getEstado()
				);
		
		dao.gravar(pf);
		
		return pf;
		
	}

}
