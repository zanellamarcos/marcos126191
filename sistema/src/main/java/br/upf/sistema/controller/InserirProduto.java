package br.upf.sistema.controller;

import br.upf.sistema.beans.ItemPedido;
import br.upf.sistema.beans.Produto;
import br.upf.sistema.dao.GenericDaoImpl;

public class InserirProduto {

	private GenericDaoImpl dao;

	
	public InserirProduto(GenericDaoImpl dao) {
		this.dao = dao;
	}
	
	
	public ItemPedido grava(Produto produto) throws Exception{
		ItemPedido p = new ItemPedido(
				produto.getId(),
				produto.getNome(),
				produto.getDescricao(),
				produto.getUnidadeMedida(),
				produto.getPrecoCompra(),
				produto.getPrecoVenda(),
				produto.getEstoque(),
				produto.getEstoqueMinimo());
		
		dao.gravar(p);
		
		return p;
		
	}

}
