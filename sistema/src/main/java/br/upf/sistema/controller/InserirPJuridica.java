package br.upf.sistema.controller;

import br.upf.sistema.beans.Cidade;
import br.upf.sistema.beans.PJuridica;
import br.upf.sistema.dao.GenericDaoImpl;

public class InserirPJuridica {

	private GenericDaoImpl dao;

	
	public InserirPJuridica(GenericDaoImpl dao) {
		this.dao = dao;
	}
	
	
	public PJuridica grava(Cidade cidade) throws Exception{
		PJuridica pj = new PJuridica(
				cidade.getId(),
				cidade.getNome(),
				cidade.getEstado()
				);
		
		dao.gravar(pj);
		
		return pj;
		
	}

}
