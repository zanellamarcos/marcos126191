package br.upf.sistema.beans;

import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity implementation class for Entity: Produto
 *
 */
@Entity
@Table(schema = "cadastros")
public class Produto implements Serializable {

	@Version
	private Long versao;

	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "genProduto")
	@SequenceGenerator(name = "genProduto", sequenceName = "genProduto", schema = "cadastros", allocationSize = 1)
	private Long id;
	@Column(unique = true, nullable = false, length = 60)
	@NotBlank(message="O nome do produto é obrigatório.")
	@Length(min=3, max=60, message="O nome do produto deve ter entre {min} e {max} caracteres.")
	private String nome;
	@Column(nullable = false, length = 150)
	@NotBlank(message = "A descrição do produto é obrigatória.")
	@Length(min = 3, max = 150, message = "A descrição do produto deve ter entre {min} e {max} caracteres.")
	private String descricao;
	@Column(length = 2, nullable = false)
	@Enumerated(EnumType.STRING)
	@NotNull(message = "O unidade de medida é obrigatória.")
	private UnidadeMedidaEnum unidadeMedida;
	@Column(nullable = false, precision = 2)
	@NotNull(message = "O preço de compra é obrigatório.")
	private Float precoCompra;
	@Column(nullable = false, precision = 2)
	@NotNull(message = "O preço de venda é obrigatório.")
	private Float precoVenda;
	@Column(nullable = false, updatable = false)
	@NotNull(message = "O estoque é obrigatório.")
	private Float estoque;
	@Column(nullable = false, updatable = false)
	@NotNull(message = "O estoque mínimo é obrigatório.")
	private Float estoqueMinimo;
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "Produto [id=" + id + ", nome=" + nome + ", descricao="
				+ descricao + ", unidadeMedida=" + unidadeMedida
				+ ", precoCompra=" + precoCompra + ", precoVenda=" + precoVenda
				+ ", estoque=" + estoque + ", estoqueMinimo=" + estoqueMinimo
				+ "]";
	}

	public Produto() {
		super();
	}

	public Produto(Long id, String nome, String descricao,
			UnidadeMedidaEnum unidadeMedida, Float precoCompra,
			Float precoVenda, Float estoque, Float estoqueMinimo) {
		super();
		this.id = id;
		this.nome = nome;
		this.descricao = descricao;
		this.unidadeMedida = unidadeMedida;
		this.precoCompra = precoCompra;
		this.precoVenda = precoVenda;
		this.estoque = estoque;
		this.estoqueMinimo = estoqueMinimo;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public UnidadeMedidaEnum getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(UnidadeMedidaEnum unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

	public Float getPrecoCompra() {
		return this.precoCompra;
	}

	public void setPrecoCompra(Float precoCompra) {
		this.precoCompra = precoCompra;
	}

	public Float getPrecoVenda() {
		return this.precoVenda;
	}

	public void setPrecoVenda(Float precoVenda) {
		this.precoVenda = precoVenda;
	}

	public Float getEstoque() {
		return this.estoque;
	}

	public void setEstoque(Float estoque) {
		this.estoque = estoque;
	}

	public Float getEstoqueMinimo() {
		return this.estoqueMinimo;
	}

	public void setEstoqueMinimo(Float estoqueMinimo) {
		this.estoqueMinimo = estoqueMinimo;
	}

	public Long getVersao() {
		return versao;
	}

	public void setVersao(Long versao) {
		this.versao = versao;
	}

}
