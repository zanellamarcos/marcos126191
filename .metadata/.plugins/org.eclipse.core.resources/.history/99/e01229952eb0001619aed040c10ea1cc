package br.upf.sistema.testes;

import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import br.upf.sistema.beans.Produto;
import br.upf.sistema.beans.UnidadeMedidaEnum;
import br.upf.sistema.dao.GenericDaoImpl;
import br.upf.sistema.util.JPAUtil;
import br.upf.sistema.util.ValidateUtil;

public class ProdutoTeste {
	
	private static Validator validator;

	@BeforeClass
	public static void inicializar() {
		validator = Validation.buildDefaultValidatorFactory().getValidator();
	}

	@Test
	public void produtoSemNomeValidator() {
		// Produto c = new Produto(id, nome, descricao, unidadeMedida,
		// precoCompra, precoVenda, estoque, estoqueMinimo);
		Produto p = new Produto(null, null, "Milho em Grãos",
				UnidadeMedidaEnum.SC, 50.00f, 60.00f, 50000.00f, 10000f);
		Set<ConstraintViolation<Produto>> erros = validator.validate(p);
		Assert.assertEquals(1, erros.size());
		Object[] vet = erros.toArray();
		Assert.assertEquals("O nome do produto e obrigatorio.",
				((ConstraintViolation<Produto>) vet[0]).getMessage());
	}
	
	@Test
	public void produtoTestaNomeValidator() {
		// Produto c = new Produto(id, nome, descricao, unidadeMedida,
		// precoCompra, precoVenda, estoque, estoqueMinimo);
		Produto p = new Produto(null, "Mi", "Milho em Grãos",
				UnidadeMedidaEnum.SC, 50.00f, 60.00f, 50000.00f, 10000f);
		Set<ConstraintViolation<Produto>> erros = validator.validate(p);
		Assert.assertEquals(1, erros.size());
		Object[] vet = erros.toArray();
		Assert.assertEquals("O nome do produto deve ter entre 3 e 60 caracteres.",
				((ConstraintViolation<Produto>) vet[0]).getMessage());
	}
	
	
	@Test
	public void produtoTestaNull() {
		//Produto p = new Produto(id, nome, descricao, unidadeMedida, precoCompra, precoVenda, estoque, estoqueMinimo);
		Produto p = new Produto(null, null, null, null, null, null, null, null);
		List<String> erros = ValidateUtil.validationToStringList(p);
		Assert.assertEquals(7, erros.size());
		Assert.assertEquals("A unidade de medida e obrigatoria.", erros.get(0));
		Assert.assertEquals("O preco de compra e obrigatorio.", erros.get(3));
		Assert.assertEquals("O nome do produto e obrigatorio.", erros.get(4));
		Assert.assertEquals("A descricao do produto e obrigatoria.", erros.get(5));
		Assert.assertEquals("O preco de venda e obrigatorio.", erros.get(2));
		Assert.assertEquals("O estoque e obrigatorio.", erros.get(1));
		Assert.assertEquals("O estoque minimo e obrigatorio.", erros.get(6));
		
	}
	
	
	@Test
	public void produtoTestaNome() {
		//Produto p = new Produto(id, nome, descricao, unidadeMedida, precoCompra, precoVenda, estoque, estoqueMinimo);
		Produto p = new Produto(null, "Mi", "Milho em Grãos", UnidadeMedidaEnum.SC, 50.00f, 60.00f, 50000.00f, 10000f);
		List<String> erros = ValidateUtil.validationToStringList(p);
		Assert.assertEquals(1, erros.size());
		Assert.assertEquals("O nome do produto deve ter entre 3 e 60 caracteres.", erros.get(0));
		//Assert.assertEquals("O nome do produto e obrigatorio.", erros.get(1));
		
	}

    
	@Test
	public void produtoTestaDescricao() {
		//Produto p = new Produto(id, nome, descricao, unidadeMedida, precoCompra, precoVenda, estoque, estoqueMinimo);
		Produto p = new Produto(null, "Milho", " ", UnidadeMedidaEnum.SC, 50.00f, 60.00f, 50000.00f, 10000f);
		List<String> erros = ValidateUtil.validationToStringList(p);
		Assert.assertEquals(2, erros.size());
		Assert.assertEquals("A descricao do produto e obrigatoria.", erros.get(0));
		Assert.assertEquals("A descricao do produto deve ter entre 3 e 150 caracteres.", erros.get(1));
	}
	
	
	@Test
	public void produtoTestaPrecos() {
		//Produto p = new Produto(id, nome, descricao, unidadeMedida, precoCompra, precoVenda, estoque, estoqueMinimo);
		Produto p = new Produto(null, "Milho", "Milho em Grão ", UnidadeMedidaEnum.SC, null, null, 50000.00f, 10000f);
		List<String> erros = ValidateUtil.validationToStringList(p);
		Assert.assertEquals(2, erros.size());
		Assert.assertEquals("O preco de compra e obrigatorio.", erros.get(1));
		Assert.assertEquals("O preco de venda e obrigatorio.", erros.get(0));
	}
	
	
	@Test
	public void produtoTestaEstoque() {
		//Produto p = new Produto(id, nome, descricao, unidadeMedida, precoCompra, precoVenda, estoque, estoqueMinimo);
		Produto p = new Produto(null, "Milho", "Milho em Grão ", UnidadeMedidaEnum.SC, 50.00f, 60.00f, null, null);
		List<String> erros = ValidateUtil.validationToStringList(p);
		Assert.assertEquals(2, erros.size());
		Assert.assertEquals("O estoque e obrigatorio.", erros.get(0));
		Assert.assertEquals("O estoque minimo e obrigatorio.", erros.get(1));
	}

	
	// TESTES  NO BANCO DE DADOS 
	@Test
	public void produtoSemNome() {
		try {
			Produto p = new Produto(null, null, "Milho em Grãos", UnidadeMedidaEnum.SC, 50.00f, 60.00f, 50000.00f, 10000f);
			EntityManager em = JPAUtil.getEntityManager();
			em.getTransaction().begin();
			em.persist(p);
			em.getTransaction().commit();
		} catch (Exception e) {
			List<String> erros = ValidateUtil.exceptionToStringList(e.getCause());
			Assert.assertEquals(1, erros.size());
			Assert.assertEquals("O nome do produto e obrigatorio.",erros.get(0));
		}
	}
	
		
	@Test
	public void inserirProduto() throws Exception {
		// produto 1
		Produto p = new Produto(null, "Milho", "Milho em Grãos",
				UnidadeMedidaEnum.SC, 50.00f, 60.00f, 50000.00f, 10000f);
		p = new GenericDaoImpl().gravar(p);
		System.out.println(p);
		// produto 2
		Produto p1 = new Produto(null, "Soja", "Soja em Grãos",
				UnidadeMedidaEnum.KG, 0.90f, 0.95f, 57000.00f, 14300f);
		p1 = new GenericDaoImpl().gravar(p1);
		System.out.println(p1);
		// produto 3
		Produto p2 = new Produto(null, "Cangica", "Cangica Moída",
				UnidadeMedidaEnum.TN, 820.00f, 870.00f, 57.00f, 14.30f);
		p2 = new GenericDaoImpl().gravar(p2);
		System.out.println(p2);
	}

	@Test
	public void listarProduto() {
		List<Produto> lista = new GenericDaoImpl().getObjetos(Produto.class);
		System.out.println(lista);
	}

	@Test
	public void excluirProduto() throws Exception {
		//Produto p = new GenericDaoImpl().getObjetoById(Produto.class, 3L);
		// Produto p = new Produto();
		new GenericDaoImpl().excluir(Produto.class, 3L);
		//System.out.println(p);
		listarProduto();
	}

	@Test
	public void alterarProduto() throws Exception {
		Produto p1 = new GenericDaoImpl().getObjetoById(Produto.class, 2L);
		p1.setNome("Trigo");
		p1.setDescricao("Trigo Moido");
		new GenericDaoImpl().gravar(p1);
		listarProduto();
	}

	@Test
	public void alterarProdutoVenda() throws Exception {
		Produto p1 = new GenericDaoImpl().getObjetoById(Produto.class, 1L);
		p1.setPrecoVenda(75.00F);
		new GenericDaoImpl().gravar(p1);
		listarProduto();
	}
	
}
