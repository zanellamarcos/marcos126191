package br.upf.ejb.testes;

import java.util.List;

import javax.naming.NamingException;

import br.upf.ejb.util.ServiceLocator;
import br.upf.pos.ecommerce.beans.Categoria;
import br.upf.pos.ecommerce.sessionbean.CategoriaBeanRemote;

public class UsarCategoriaBeanEJB {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        try {
			CategoriaBeanRemote ejb = (CategoriaBeanRemote) 
					              ServiceLocator.buscarEJB(CategoriaBeanRemote.class);
		    
			Categoria c = new Categoria(6L, "Roteadores");
			ejb.inserir(c);
			
			
			List<Categoria> list = ejb.getCategorias();
		    System.out.println(list);
        
        
        } catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        
	}

}
