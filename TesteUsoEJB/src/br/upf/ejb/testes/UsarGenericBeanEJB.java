package br.upf.ejb.testes;

import java.util.List;

import javax.naming.NamingException;

import br.upf.ejb.util.ServiceLocator;
import br.upf.pos.ecommerce.beans.Categoria;
import br.upf.pos.ecommerce.beans.Produto;
import br.upf.pos.ecommerce.sessionbean.GenericBeanRemote;

public class UsarGenericBeanEJB {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        try {
			GenericBeanRemote ejb = (GenericBeanRemote) 
					              ServiceLocator.buscarEJB(GenericBeanRemote.class);
		   
			/*
			Categoria c = new Categoria(6L, "Roteadores");
			c = ejb.gravar(c);
			*/
			
			List<Categoria> list = ejb.getObjetos(Categoria.class);
		    System.out.println(list);
        
			Produto p = new Produto(null, "Produto 39", 2342f, 
					                "Produto 39", 0, list.get(0));
			p = ejb.gravar(p);
			
			List<Produto> listP = ejb.getObjetos(Produto.class);
		    System.out.println(listP);
			
        
        } catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        
	}

}
