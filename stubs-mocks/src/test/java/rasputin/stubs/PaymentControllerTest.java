package rasputin.stubs;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import org.junit.Test;

public class PaymentControllerTest {
	
	// This test use a mock
	@Test
	public void shouldCreateNewPayment(){
		PaymentApiService serviceMock = mock(PaymentApiService.class);
		PaymentController pc = new PaymentController(serviceMock);
		
		pc.createNewPayment(100);
		
		verify(serviceMock).addPayment(100);
	}
	
	// This test use a stub
	@Test
	public void shouldReturnTotalAmountOfPayments(){
		PaymentApiService serviceMock = mock(PaymentApiService.class);
		PaymentController pc = new PaymentController(serviceMock);
		
		Collection<Integer> fakeCollectionOfPayments =
				Collections.unmodifiableCollection(Arrays.asList(10,20));
		
		when(serviceMock.getAllPayments()).thenReturn(fakeCollectionOfPayments);
		
		assertEquals(30, pc.amountPayments());
	}

}
