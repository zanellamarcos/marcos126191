package rasputin.stubs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class PaymentApiService {
	
	private List<Integer> payments = new ArrayList<Integer>();

	public void addPayment(int value) {
		System.out.println("Talking with the outside world");
		System.out.println("Adding payment " + value);
		payments.add(value);
	}

	public Collection<Integer> getAllPayments() {
		return Collections.unmodifiableCollection(payments);
	}

}
