package rasputin.stubs;

import java.util.concurrent.atomic.LongAdder;

public class PaymentController {
	
	private PaymentApiService pas;
	
	public PaymentController(PaymentApiService pas){
		this.pas = pas;
	}
	
	public void createNewPayment(int value){
		this.pas.addPayment(value);
	}
	
	public int amountPayments(){
		LongAdder accu = new LongAdder();
		this.pas.getAllPayments().forEach(accu::add);
		
		return accu.intValue();
	}

}
