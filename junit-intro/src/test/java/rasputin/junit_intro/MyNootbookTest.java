package rasputin.junit_intro;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MyNootbookTest {
	
	private MyNotbook notebook;
	
	
	@Before
	public void setUp(){
		notebook = new MyNotbook();
	}
	
	@Test
	public void myNotebookShouldInitializeEmpty(){		
		assertTrue(notebook.getNotes().isEmpty());
	}
	
	@Test
	public void shouldAddNewNote(){
		notebook.addNote("Comprar café");
		assertEquals("Comprar café", notebook.getNotes().get(0));
	}
	
	@Test
	public void shouldRemoteAnote(){
		notebook.addNote("Comprar café");
		notebook.removeNote(0);
		
		assertTrue(notebook.getNotes().isEmpty());
	}
	
	

}
