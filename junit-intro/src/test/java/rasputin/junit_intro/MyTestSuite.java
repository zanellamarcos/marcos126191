package rasputin.junit_intro;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({MathTest.class, MyNootbookTest.class})
public class MyTestSuite {
	
}
