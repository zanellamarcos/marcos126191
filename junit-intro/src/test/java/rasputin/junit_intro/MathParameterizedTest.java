package rasputin.junit_intro;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class MathParameterizedTest {
	private int expected;
	private int firstNumber;
	private int secondNumber;
	
	public MathParameterizedTest(int exptectedResult, int firstNumber, int secondNumber){
		this.expected = exptectedResult;
		this.firstNumber = firstNumber;
		this.secondNumber = secondNumber;
	}
	
	@Parameters
	public static Collection<Integer[]> SumNumbers(){
		return Arrays.asList(new Integer[][] {{4, 2,2}, {100, 50, 50}, {40, 10, 30}});
	}
	
	@Test
	public void sumNumbersTest(){
		Math math = new Math();
		
		assertEquals(expected, math.sum(firstNumber, secondNumber));
	}
	
}
