package tdd_triangulation;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;


public class ListAggregateTest {
	
	@Test
	public void shouldSumAllElementsInList(){
		ListAggregate la = new ListAggregate();
		
		List<Integer> elements = Arrays.asList(1,2,3,4,5);
		
		Assert.assertEquals(15, la.sumAllElements(elements));
	}
}
