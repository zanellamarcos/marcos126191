package rasputin.junit_intro;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MyNotbook {
	
	private List<String> notes = new ArrayList<String>();
	
	public void addNote(String note){
		notes.add(note);
	}
	
	public void removeNote(int noteIndex){
		notes.remove(noteIndex);
	}
	
	public List<String> getNotes(){
		return Collections.unmodifiableList(notes);
	}

}
