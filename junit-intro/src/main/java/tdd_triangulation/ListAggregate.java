package tdd_triangulation;

import java.util.List;

public class ListAggregate {


	public int sumAllElements(List<Integer> elements) {
		int sum = 0;
		
		for (Integer element : elements) 
			sum += element;
		
		return sum;
	}
}
