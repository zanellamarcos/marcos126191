package br.upf.pedidos.ejb.util;

import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;

public class ServiceLocator {
	private static String host = "localhost"; 
	private static String porta = "3700"; 
	   
	  // Atributo de propriedades para encontrar o Glassfish. 
	  private static Properties properties = null; 
	 
	  /** 
	   * M�todo gen�rico para fazer lookup de servi�os EJB a partir de sua interface. 
	   * @param <T> Um tipo gen�rico que representar� a classe do EJB buscado.   
	   * @param interfaceEJBName interface do EJB que ser� feito o lookup. 
	   * @return a inst�ncia do EJB encontrado no servidor. 
	   * @throws NamingException Erro caso ocorra.  
	   */ 
	  public static <T> T buscarEJB(Class<T> interfaceEJBName) throws NamingException  { 
	    // Se ainda n�o inicializou as propriedades inicializar s� no primeiro uso 
	    if (properties == null) { 
	      properties = new Properties(); 
	      properties.put("org.omg.CORBA.ORBInitialHost", host); 
	      properties.put("org.omg.CORBA.ORBInitialPort", porta); 
	    } 
	    // Cria o initial context para buscar o EJB via JNDI. 
	    InitialContext ctx = new InitialContext(properties); 
	    // Obt�m o nome completo da interface utilizando reflection, faz o lookup e retorna o EJB. 
	    return (T) ctx.lookup(interfaceEJBName.getName()); 
	  } 
}
