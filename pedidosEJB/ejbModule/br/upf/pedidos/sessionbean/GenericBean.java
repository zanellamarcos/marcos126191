package br.upf.pedidos.sessionbean;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.upf.pedidos.dao.GenericDaoEjbImpl;

/**
 * Session Bean implementation class GenericBean
 */
@Stateless
@LocalBean
public class GenericBean implements GenericBeanRemote {

	@PersistenceContext(unitName="pedidos")
	private EntityManager em;
	
    /**
     * Default constructor. 
     */
    public GenericBean() {
        // TODO Auto-generated constructor stub
    }
    
    
    @Override
	public <T> List<T> getObjetos(Class<T> classe) {
		return new GenericDaoEjbImpl(em).getObjetos(classe);
	}

	@Override
	public <T> T gravar(T c) throws Exception {
	   return new GenericDaoEjbImpl(em).gravar(c);
	}

}
