package br.upf.pedidos.desenvolvimento;

import javax.persistence.EntityManager;

import org.junit.Test;

import br.upf.pedidos.beans.Entrega;
import br.upf.pedidos.beans.PagamentoEnum;
import br.upf.pedidos.beans.Pedido;
import br.upf.pedidos.beans.Pessoa;
import br.upf.pedidos.util.JPAUtil;

public class PedidoTeste {
	
	@Test
	public void inserir(){
		EntityManager em = JPAUtil.getEntityManager();
		//Pedido
		Pedido p = new Pedido();
		p.setPessoa(em.find(Pessoa.class,1L));
		p.setEntrega(em.find(Entrega.class,2L));
		p.setPagamento(PagamentoEnum.DINHEIRO);
		
		
		
		
		//PERSISTINDO
		em.getTransaction().begin();
		em.persist(p);
		em.getTransaction().commit();
		
	}

}
