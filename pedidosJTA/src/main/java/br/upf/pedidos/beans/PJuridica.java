package br.upf.pedidos.beans;

import java.io.Serializable;
import java.lang.String;

import javax.persistence.*;

import org.hibernate.validator.constraints.br.CNPJ;

/**
 * Entity implementation class for Entity: PJuridica
 *
 */
@Entity
@Table(schema = "cadastros")
public class PJuridica extends Pessoa implements Serializable {

	@Column(length = 18, unique = true)
	// 99.999.999/9999-99
	@CNPJ(message = "CNPJ é invalido.")
	private String cnpj;
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "PJuridica [cnpj=" + cnpj + "]";
	}

	public PJuridica() {
		super();
	}

	public PJuridica(String cnpj) {
		super();
		this.cnpj = cnpj;
	}

	public String getCnpj() {
		return this.cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

}
