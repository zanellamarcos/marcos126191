package br.upf.pedidos.beans;

import static javax.persistence.GenerationType.SEQUENCE;
import static javax.persistence.InheritanceType.JOINED;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity implementation class for Entity: Pessoa
 *
 */
@Entity
@Inheritance(strategy = JOINED)
@Table(schema = "cadastros")
public abstract class Pessoa implements Serializable {

	@Version
	private Long versao;

	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "genPessoa")
	@SequenceGenerator(name = "genPessoa", sequenceName = "genPessoa", schema = "cadastros", allocationSize = 1)
	private Long id;
	@Column(nullable = false, length = 60)
	@NotBlank(message = "O nome da pessoa é obrigatória.")
	@Length(min = 3, max = 60, message = "A pessoa deve ter entre {min} e {max} caracteres.")
	private String nome;
	@Column(length = 14, nullable = false)
	@NotBlank(message = "O Incrição Estadual é obrigatória.")
	@Length(min = 4, max = 14, message = "A pessoa deve ter entre {min} e {max} caracteres.")
	private String IE;
	@Column(length = 60, nullable = false)
	@NotBlank(message = "O endereço da pessoa é obrigatória.")
	@Length(min = 3, max = 60, message = "O endereço deve ter entre {min} e {max} caracteres.")
	private String endereco;
	@Column(length = 10, nullable = false)
	@NotBlank(message = "O numero é obrigatório.")
	@Length(min = 1, max = 10, message = "O numero deve ter entre {min} e {max} caracteres.")
	private String numero;
	@Column(length = 30)
	@Length(min = 3, max = 30, message = "O complemento deve ter entre {min} e {max} caracteres.")
	private String complemento;
	@Column(length = 50, nullable = false)
	@NotBlank(message = "O bairro é obrigatório.")
	@Length(min = 3, max = 50, message = "O bairro deve ter entre {min} e {max} caracteres.")
	private String bairro;
	@ManyToOne(optional = false)
	@NotNull(message = "A cidade da pessoa é obrigatória.")
	private Cidade cidade;
	@Column(length = 10, nullable = false)
	@NotNull(message = "O CEP é obrigatório.")
	private String CEP;
	@Column(length = 20, nullable = false)
	@NotNull(message = "O Fone é obrigatório.")
	private String fone;
	@Column(unique = true, nullable = false, length = 80)
	@Email(message = "O Email inválido.")
	private String email;
	@Column(nullable = false, length = 50)
	@NotBlank(message = "A senha é obrigatória.")
	@Length(min = 6, max = 50, message = "A senha deve ter entre {min} e {max} caracteres.")
	private String senha;
	@Column(nullable = false)
	@NotNull(message = "A situação de ser informada.")
	private Boolean situacao;
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "Pessoa [id=" + id + ", nome=" + nome + ", IE=" + IE
				+ ", endereco=" + endereco + ", numero=" + numero
				+ ", complemento=" + complemento + ", bairro=" + bairro
				+ ", cidade=" + cidade + ", CEP=" + CEP + ", fone=" + fone
				+ ", email=" + email + ", senha=" + senha + ", situacao="
				+ situacao + "]";
	}

	public Pessoa() {
		super();
	}

	
	public Pessoa(Long id, String nome, String iE, String endereco,
			String numero, String complemento, String bairro, Cidade cidade,
			String cEP, String fone, String email, String senha,
			Boolean situacao) {
		super();
		this.id = id;
		this.nome = nome;
		IE = iE;
		this.endereco = endereco;
		this.numero = numero;
		this.complemento = complemento;
		this.bairro = bairro;
		this.cidade = cidade;
		CEP = cEP;
		this.fone = fone;
		this.email = email;
		this.senha = senha;
		this.situacao = situacao;
	}

	public String getBairro() {
		return this.bairro;
	}

	public String getCEP() {
		return this.CEP;
	}

	public Cidade getCidade() {
		return this.cidade;
	}

	public String getComplemento() {
		return this.complemento;
	}

	public String getEmail() {
		return this.email;
	}

	public String getEndereco() {
		return this.endereco;
	}

	public String getFone() {
		return this.fone;
	}

	public Long getId() {
		return this.id;
	}

	public String getIE() {
		return this.IE;
	}

	public String getNome() {
		return this.nome;
	}

	public String getNumero() {
		return numero;
	}

	public String getSenha() {
		return this.senha;
	}

	public Boolean getSituacao() {
		return this.situacao;
	}

	public Long getVersao() {
		return versao;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public void setCEP(String CEP) {
		this.CEP = CEP;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public void setFone(String fone) {
		this.fone = fone;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setIE(String IE) {
		this.IE = IE;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public void setSituacao(Boolean situacao) {
		this.situacao = situacao;
	}

	public void setVersao(Long versao) {
		this.versao = versao;
	}

}
