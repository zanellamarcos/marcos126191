package br.upf.pos.ecommerce;

import java.util.List;

import org.junit.Test;

import br.upf.pos.ecommerce.beans.Administrador;
import br.upf.pos.ecommerce.beans.Categoria;
import br.upf.pos.ecommerce.beans.Produto;
import br.upf.pos.ecommerce.dao.GenericDaoImpl;

public class TesteGenericDao {

	@Test
	public void listarCategoria(){
		List<Categoria> lista = new GenericDaoImpl().getObjetos(Categoria.class);
		System.out.println(lista);
	}
	
	@Test
	public void listarProdutos(){
		List<Produto> lista = new GenericDaoImpl().getObjetos(Produto.class);
		System.out.println(lista);
	}
	
	@Test
	public void listarAdministradores(){
		List<Administrador> lista = new GenericDaoImpl().getObjetos(Administrador.class);
		System.out.println(lista);
	}
	
	@Test
	public void inserirProduto() throws Exception{
		Categoria c = new GenericDaoImpl().getObjetoById(Categoria.class, 3L);
		Produto p = new Produto(null, "Produto novo teste 222", 23.5f, "teste", 0, c);
		p = new GenericDaoImpl().gravar(p);
		System.out.println(p);
	}
	
	@Test
	public void testeVersion() throws Exception{
		Categoria c1 = new GenericDaoImpl().getObjetoById(Categoria.class, 3L);
		Categoria c2 = new GenericDaoImpl().getObjetoById(Categoria.class, 3L);
		
		c1.setDescricao("Alterado pelo user 1");
		c2.setDescricao("Alterado pelo user 2");
		
		new GenericDaoImpl().gravar(c1);
		new GenericDaoImpl().gravar(c2);
		
	}
	
	
	
}
