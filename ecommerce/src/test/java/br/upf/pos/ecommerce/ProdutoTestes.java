package br.upf.pos.ecommerce;

import javax.persistence.EntityManager;

import org.junit.Test;

import br.upf.pos.ecommerce.beans.Categoria;
import br.upf.pos.ecommerce.beans.Produto;
import br.upf.pos.ecommerce.util.JPAUtil;

public class ProdutoTestes {

	@Test
	public void inserir(){
		EntityManager em = JPAUtil.getEntityManager();
		
		Produto p = new Produto();
		p.setNome("Teclado sem fio ...");
		p.setDescricao("descrição ...");
		p.setPreco(123.50F);
		p.setQuantidadeEstoque(0);
		p.setCategoria(em.find(Categoria.class, 1L));
		em.getTransaction().begin();
		em.persist(p);
		em.getTransaction().commit();
	}
	
	
	
	
}
