package br.upf.pos.ecommerce.beans;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * Entity implementation class for Entity: Compra
 *
 */
@Entity
@Table(schema = "compras")
public class Compra implements Serializable {
	@Version
	private Long versao;

	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "genCompra")
	@SequenceGenerator(name = "genCompra", sequenceName = "genCompra", schema = "compras", allocationSize = 1)
	private Long id;
	@Column(nullable=false)
	private Float total;
	@Enumerated(EnumType.ORDINAL)
	private FormaPgtoEnum formaPagamento;
	@Temporal(TemporalType.DATE)
	private Date data;
	@ManyToOne(optional = false)
	private Cliente cliente;
	@ManyToOne(fetch = LAZY)
	private CupomDesconto cupomDesconto;
	@OneToMany(cascade = ALL, orphanRemoval = true, mappedBy = "compra", fetch = LAZY)
	@OrderColumn(name = "id")
	private List<CompraItem> itens;
	private static final long serialVersionUID = 1L;

	public Compra() {
		super();
	}   
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public Float getTotal() {
		return this.total;
	}

	public void setTotal(Float total) {
		this.total = total;
	}   
	public FormaPgtoEnum getFormaPagamento() {
		return this.formaPagamento;
	}

	public void setFormaPagamento(FormaPgtoEnum formaPagamento) {
		this.formaPagamento = formaPagamento;
	}   
	public Date getData() {
		return this.data;
	}

	public void setData(Date data) {
		this.data = data;
	}   
 
	public CupomDesconto getCupomDesconto() {
		return this.cupomDesconto;
	}

	public void setCupomDesconto(CupomDesconto cupomDesconto) {
		this.cupomDesconto = cupomDesconto;
	}   
	public List<CompraItem> getItens() {
		return this.itens;
	}

	public void setItens(List<CompraItem> itens) {
		this.itens = itens;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Long getVersao() {
		return versao;
	}
	public void setVersao(Long versao) {
		this.versao = versao;
	}
		
}
