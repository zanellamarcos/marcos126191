package br.upf.pos.ecommerce.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.upf.pos.ecommerce.beans.Categoria;
import br.upf.pos.ecommerce.util.JPAUtil;

public class CategoriaDaoImpl implements CategoriaDao {

	private EntityManager em;
	
	public CategoriaDaoImpl() {
		super();
		em = JPAUtil.getEntityManager();
	}

	@Override
	public Categoria gravar(Categoria objeto) throws Exception {
		em.getTransaction().begin();
		objeto = em.merge(objeto);
		em.getTransaction().commit();
		em.close();
		return objeto;
	}

	@Override
	public void excluir(Long id) throws Exception {
		em.getTransaction().begin();
		em.remove(em.find(Categoria.class, id));
		em.getTransaction().commit();
		em.close();
	}

	@Override
	public Categoria getObjetoById(Long id) {
		Categoria ret = em.find(Categoria.class, id);
		em.close();
		return ret;
	}

	@Override
	public List<Categoria> getObjetos() {
		List<Categoria> ret = em.createQuery("select o from Categoria o").getResultList();
		em.close();
		return ret;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Categoria> getObjetos(String atributoOrdem) {
		List<Categoria> ret = em.createQuery(
				"select o from Categoria o order by "+atributoOrdem).getResultList();
		em.close();
		return ret;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Categoria> getObjetos(String atributoOrdem, int maxResults,
			int firstResult) {
		List<Categoria> ret = em.createQuery(
				"select o from Categoria o order by "+atributoOrdem).
				setMaxResults(maxResults).setFirstResult(firstResult).getResultList();
		em.close();
		return ret;
	}

}
