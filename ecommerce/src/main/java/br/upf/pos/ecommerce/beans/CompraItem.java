package br.upf.pos.ecommerce.beans;

import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;
import java.lang.Float;
import java.lang.Integer;
import java.lang.Long;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: CompraItem
 *
 */
@Entity
@Table(schema = "compras")
public class CompraItem implements Serializable {
	@Version
	private Long versao;
	
	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "genCompraItem")
	@SequenceGenerator(name = "genCompraItem", sequenceName = "genCompraItem", schema = "compras", allocationSize = 1)
	private Long id;
	@Column(nullable=false)
	private Integer quantidade;
	@Column(nullable=false, precision=2)
	private Float preco;
	@Column(nullable=false, precision=2)
	private Float total;
	@ManyToOne(optional = false)
	private Produto produto;
	@ManyToOne(optional = false)
	private Compra compra;
	
	private static final long serialVersionUID = 1L;

	public CompraItem() {
		super();
	}   
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public Integer getQuantidade() {
		return this.quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}   
	public Float getPreco() {
		return this.preco;
	}

	public void setPreco(Float preco) {
		this.preco = preco;
	}   
	public Float getTotal() {
		return this.total;
	}

	public void setTotal(Float total) {
		this.total = total;
	}   
	public Produto getProduto() {
		return this.produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public Compra getCompra() {
		return compra;
	}
	public void setCompra(Compra compra) {
		this.compra = compra;
	}
	public Long getVersao() {
		return versao;
	}
	public void setVersao(Long versao) {
		this.versao = versao;
	}   
	   
}
