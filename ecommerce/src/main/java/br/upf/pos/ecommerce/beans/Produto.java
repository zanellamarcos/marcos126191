package br.upf.pos.ecommerce.beans;

import java.io.Serializable;
import java.lang.Float;
import java.lang.Integer;
import java.lang.Long;
import java.lang.String;

import javax.persistence.*;

import static javax.persistence.GenerationType.SEQUENCE;

/**
 * Entity implementation class for Entity: Produto
 *
 */
@Entity
@Table(schema = "cadastros")
public class Produto implements Serializable {
	@Version
	private Long versao;
	   
	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "genProduto")
	@SequenceGenerator(name = "genProduto", sequenceName = "genProduto", schema = "cadastros", allocationSize = 1)
	private Long id;
	@Column(unique = true, nullable = false, length = 60)
	private String nome;
	@Column(nullable = false)
	private Float preco;
	@Lob
	private String descricao;
	@Column(nullable = false, updatable = false)
	private Integer quantidadeEstoque;
	
	@ManyToOne(optional = false)
	private Categoria categoria;
	private static final long serialVersionUID = 1L;

	public Produto() {
		super();
		quantidadeEstoque = 0;
	}
	
	
	
	public Produto(Long id, String nome, Float preco, String descricao,
			Integer quantidadeEstoque, Categoria categoria) {
		super();
		this.id = id;
		this.nome = nome;
		this.preco = preco;
		this.descricao = descricao;
		this.quantidadeEstoque = quantidadeEstoque;
		this.categoria = categoria;
	}

	
	
	@Override
	public String toString() {
		return "Produto [id=" + id + ", nome=" + nome + ", preco=" + preco
				+ "]";
	}



	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}   
	public Float getPreco() {
		return this.preco;
	}

	public void setPreco(Float preco) {
		this.preco = preco;
	}   
	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}   
	public Integer getQuantidadeEstoque() {
		return this.quantidadeEstoque;
	}

	public void setQuantidadeEstoque(Integer quantidadeEstoque) {
		this.quantidadeEstoque = quantidadeEstoque;
	}   
	public Categoria getCategoria() {
		return this.categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public Long getVersao() {
		return versao;
	}
	public void setVersao(Long versao) {
		this.versao = versao;
	}

   
}
